#!/usr/bin/env bash

#### Example 1
# network_phy="Poisson"
# network_info="Poisson"
# model="SIR"


# for gamma in $(seq 0.1 0.1 1)
# do 
#     (
#         python3 driver.py --model $model --gamma_anti $gamma --gamma_pro $gamma --network_type_phy $network_phy --network_type_info $network_info  --N 10000  --num_sim 100

#     )&
# done


# #### Example 2
# network_phy="Correlated"
# network_info="Correlated"
# k1_phy_prob=0.5
# k1_info_prob=0.5
# corr_a_intra=0
# influence_anti=1
# influence_pro=0.1
# corr_a_inter=0.16
# model="SIR"

# for corr_a_inter in $(seq 0 0.05 0.5)
# do
#     (
#         python3 driver.py --model $model --corr_a_inter $corr_a_inter --corr_a_phy $corr_a_intra --corr_a_info $corr_a_intra --influence_anti $influence_anti --influence_pro $influence_pro --network_type_phy $network_phy --network_type_info $network_info  --N 10000  --num_sim 100

#     )&
#done



#### Example 3
network_phy="Poisson"
network_info="Poisson"
model="SIRS"
gamma=2
beta=2


for tao in $(seq 0 0.5 5)
do 
    (
        python3 driver.py --model $model --tao $tao --gamma_anti $gamma --gamma_pro $gamma --beta_pro $beta --beta_anti $beta --network_type_phy $network_phy --network_type_info $network_info  --N 10000  --num_sim 100

    )&
done