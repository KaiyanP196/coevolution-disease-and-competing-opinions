import matplotlib
import numpy as np
matplotlib.use('agg')
import scipy.io as sio
from collections import Counter


def initialization(N, init_size, label_phy, label_info):
    """
    param init_size = [num. nodes pro-infected, num. nodes anti-infected, num. nodes disease-infected]
    label_info = {'U': 0, 'P': 1, 'A': 2, 'R': 3}
    param label_info = {'S':0, 'P':1, 'A':2, 'R':3}
    """
    x0 = np.zeros((2, N), dtype=int)  # '0' = 'SN', 'U'

    info_inf = np.random.choice(N, init_size[0] + init_size[1],
                                replace=False)  # pick random indices to initialize as pro- & anti-infected (disjoint)
    x0[1, info_inf[0:init_size[0]]] = label_info['P']
    x0[0, info_inf[0:init_size[0]]] = label_phy['SP']
    x0[1, info_inf[init_size[0]:]] = label_info['A']
    x0[0, info_inf[init_size[0]:]] = label_phy['SA']

    phy_inf = np.random.choice(N, init_size[2], replace=False)
    x0[0, phy_inf] = label_phy[
        'I']  # overwrite phy. label for infecteds, awareness only affects susceptibles (overlap between initialized pro-/anti-s.d. & infected is allowed)

    # 2-row array of initial states in both layers
    return x0


def post_process(x0, ts, i_ind, j_ind, label, N):
    """
    StateCount: size of each compartment at each timestamp

    param label = {'phy':label_phy, 'info':label_info, 'all':label_all}
        label_phy = {'SN':0, 'SP':1, 'SA':2, 'I':3, 'R':4}
        label_info = {'U':0, 'P':1, 'A':2, 'R':3}
        label_all = {'00':0, '03':1, '11':2, '22':3, '30':4, '31':5, '32':6, '33':7, '40':8, '41':9, '42':10, '43':11} (note: some combinations of phy/info states with SP/SA are invalid)
    """
    StateCount = np.zeros((len(label['all']), len(ts) + 1))  # compartments x events
    label_all = label['all']

    # initialize compartment sizes
    c = Counter()
    for node in range(N):
        state = str(x0[0, node]) + str(x0[1, node])
        c[label_all[state]] += 1
    StateCount[list(c.keys()), 0] = list(c.values())

    for t in range(len(ts) - 1):
        StateCount[:, t + 1] = StateCount[:, t]  # get prev. state count
        StateCount[label_all[i_ind[t]], t + 1] -= 1  # node has left old compartment
        StateCount[label_all[j_ind[t]], t + 1] += 1  # node has joined new compartment

    return StateCount


def save_results(model_name, stateCount, epidemic_size, T, file_name=None, full_records=None, verbose_records=None):
    # Physical layer compartments
    s_phy = np.sum(stateCount[0:4, :], axis=0)
    i_phy = np.sum(stateCount[4:8, :], axis=0)
    r_phy = np.sum(stateCount[8:12, :], axis=0)


    # Information layer compartments
    p_info = np.sum(stateCount[[2, 5, 9], :], axis=0)
    a_info = np.sum(stateCount[[3, 6, 10], :], axis=0)
    n_info = np.sum(stateCount[[0, 1, 4, 7, 8, 11], :], axis=0)
    u_info = np.sum(stateCount[[0, 4, 8], :], axis=0)
    r_info = np.sum(stateCount[[1, 7, 11], :], axis=0)

    physical_layer = {'S': s_phy, 'I': i_phy, 'R': r_phy, 'T': T}
    info_layer = {'U': u_info, 'P': p_info, 'A': a_info, 'R': r_info, 'N': n_info, 'T': T}
    simulation_record = {'phy': physical_layer, 'info': info_layer, 'epidemic_size': epidemic_size}

    if full_records:
        simulation_record['full_records'] = full_records

    if verbose_records:
        simulation_record['verbose_records'] = verbose_records

    sio.savemat('model_' + model_name + '_simulation_' + file_name + '.mat', simulation_record)

