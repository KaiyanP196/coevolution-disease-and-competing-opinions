from simulation import *
from utility import *
import argparse
from datetime import datetime
import sys

def parse_args():
    parser = argparse.ArgumentParser(description="Read in input and output filenames.")

    parser.add_argument("--model", default="SIRS", nargs="?",
                        help="Choose the model: SIR or SIRS", type=str)

    # Parameters for dynamics
    parser.add_argument("--beta_phy", default=0.6, nargs="?", help="Base disease transmission rate",
                        type=float)
    parser.add_argument("--beta_pro", default=0.6, nargs="?",
                        help="The transmission rate for the pro opinion",
                        type=float)
    parser.add_argument("--gamma_pro", default=0.1, nargs="?",
                        help="The recovery rate for the pro opinion",
                        type=float)
    parser.add_argument("--beta_anti", default=0.6, nargs="?",
                        help="The transmission rate for the anti opinion",
                        type=float)
    parser.add_argument("--gamma_anti", default=0.1, nargs="?",
                        help="The recovery rate for the anti opinion",
                        type=float)
    parser.add_argument("--gamma_phy", default=1, nargs="?", help="The disease recovery rate", type=float)
    parser.add_argument("--tao", default=0.5, nargs="?",
                        help="Conversion rate (R - U) in the information layer", type=float)
    parser.add_argument("--influence_pro", default=0.1, nargs="?",
                        help="The influence of pro opinion on the disease transmission rate", type=float)
    parser.add_argument("--influence_anti", default=10, nargs="?",
                        help="The influence of anti opinion on the disease transmission rate", type=float)

    # Parameters for random graph models
    parser.add_argument("--network_type_phy", default="Correlated", nargs="?",
                        help="network structure for the physical layer", type=str)
    parser.add_argument("--network_type_info", default="Correlated", nargs="?",
                        help="network structure for the information layer", type=str)

    # Parameters for multiplex networks where each layer has two different degrees.
    parser.add_argument("--corr_a_phy", default=0, nargs="?",
                        help="The (1,1) entry of the intra-layer degree-degree correlation matrix for a network with only two different degrees for the physical layer",
                        type=float)
    parser.add_argument("--corr_a_info", default=0, nargs="?",
                        help="The (1,1) entry of the intra-layer degree-degree correlation matrix for a network with only two different degrees for the information layer",
                        type=float)
    parser.add_argument("--corr_a_inter", default=0, nargs="?",
                        help="The (1,1) entry of the inter layer correlation matrix", type=float)
    parser.add_argument("--k1_phy", default=2, nargs="?",
                        help="The first degree of a network with two degrees for the physical layer", type=int)
    parser.add_argument("--k2_phy", default=8, nargs="?",
                        help="The second degree of a network with two degrees for the physical layer", type=int)
    parser.add_argument("--k1_phy_prob", default=0.5, nargs="?",
                        help="The probability of seeing the first degree for the physical layer", type=float)
    parser.add_argument("--k1_info", default=2, nargs="?",
                        help="The first degree of a network with two degrees for the information layer", type=int)
    parser.add_argument("--k2_info", default=8, nargs="?",
                        help="The second degree of a network with two degrees for the information layer ", type=int)
    parser.add_argument("--k1_info_prob", default=0.5, nargs="?",
                        help="The probability of seeing the first degree for the information layer", type=float)


    # Other parameters
    parser.add_argument("--N", default=1000, nargs="?", help="Number of nodes", type=int)
    parser.add_argument("--initial_infected_phy", default=0.01, nargs="?",
                        help="Fraction of nodes in state I in the physical layer", type=float)
    parser.add_argument("--initial_infected_pro", default=0.005, nargs="?",
                        help="Fraction of nodes in state P in the information layer", type=float)
    parser.add_argument("--initial_infected_anti", default=0.005, nargs="?",
                        help="Fraction of nodes in state A iin the information layer", type=float)
    parser.add_argument("--RunTime", default=20, nargs="?", help="Maximal amount of time for running the dynamics",
                        type=float)
    parser.add_argument("--step", default=0.01, nargs="?", help="The time step for recording the simulation results",
                        type=float)
    parser.add_argument("--num_sim", default=2, nargs="?", help="Number of simulations", type=int)
    parser.add_argument('--verbose', dest='verbose', action='store_true', help="Whether to record every state transition")
    parser.add_argument('--full_records', dest='full_records', action='store_true', help="whether to record dynamics for every simulation")
    parser.set_defaults(verbose=False)
    parser.set_defaults(full_records=False)

    return parser.parse_args()


def get_param_SIR(beta_phy, gamma_phy, beta_pro, gamma_pro, beta_anti, gamma_anti, influence_pro, influence_anti, label_phy,
             label_info):
    M_phy = len(label_phy)  # physical compartments
    # nodal transitions
    r_spon_phy = np.zeros((M_phy, M_phy))
    r_spon_phy[label_phy['I'], label_phy['R']] = gamma_phy  # physical recovery
    # contact transitions
    r_cont_phy = np.zeros((M_phy, M_phy))
    r_cont_phy[label_phy['SN'], label_phy['I']] = beta_phy  # physical infection
    r_cont_phy[label_phy['SP'], label_phy[
        'I']] = beta_phy * influence_pro  # reduced infection rate for pro-susceptible nodes, 0 <= influence_pro <= 1
    r_cont_phy[label_phy['SA'], label_phy[
        'I']] = beta_phy * influence_anti  # increased infection rate for anti-susceptible nodes, influence_anti >= 1

    M_info = len(label_info)  # info compartments
    # nodal transitions
    r_spon_info = np.zeros((M_info, M_info))
    r_spon_info[label_info['P'], label_info['R']] = gamma_pro  # pro recovery
    r_spon_info[label_info['A'], label_info['R']] = gamma_anti  # anti recovery
    # contact transitions
    r_cont_info = np.zeros((M_info, M_info))
    r_cont_info[label_info['U'], label_info['P']] = beta_pro  # pro infection
    r_cont_info[label_info['U'], label_info['A']] = beta_anti  # anti infection

    return [r_cont_phy, r_spon_phy, r_cont_info, r_spon_info]


def get_param_SIRS(beta_phy, gamma_phy, beta_pro, gamma_pro, beta_anti, gamma_anti, tao, influence_pro, influence_anti, label_phy,
             label_info):
    M_phy = len(label_phy)  # physical compartments
    # nodal transitions
    r_spon_phy = np.zeros((M_phy, M_phy))
    r_spon_phy[label_phy['I'], label_phy['R']] = gamma_phy  # physical recovery
    # contact transitions
    r_cont_phy = np.zeros((M_phy, M_phy))
    r_cont_phy[label_phy['SN'], label_phy['I']] = beta_phy  # physical infection
    r_cont_phy[label_phy['SP'], label_phy[
        'I']] = beta_phy * influence_pro  # reduced infection rate for pro-susceptible nodes, 0 <= influence_pro <= 1
    r_cont_phy[label_phy['SA'], label_phy[
        'I']] = beta_phy * influence_anti  # increased infection rate for anti-susceptible nodes, influence_anti >= 1

    M_info = len(label_info)  # info compartments
    # nodal transitions
    r_spon_info = np.zeros((M_info, M_info))
    r_spon_info[label_info['P'], label_info['R']] = gamma_pro  # pro recovery
    r_spon_info[label_info['A'], label_info['R']] = gamma_anti  # anti recovery
    r_spon_info[label_info['R'], label_info['U']] = tao
    # contact transitions
    r_cont_info = np.zeros((M_info, M_info))
    r_cont_info[label_info['U'], label_info['P']] = beta_pro  # pro infection
    r_cont_info[label_info['U'], label_info['A']] = beta_anti  # anti infection

    return [r_cont_phy, r_spon_phy, r_cont_info, r_spon_info]


if __name__ == "__main__":
    args = parse_args()


    label_phy = {'SN': 0, 'SP': 1, 'SA': 2, 'I': 3, 'R': 4}
    label_info = {'U': 0, 'P': 1, 'A': 2, 'R': 3}
    label_all = {'00': 0, '03': 1, '11': 2, '22': 3, '30': 4, '31': 5, '32': 6, '33': 7, '40': 8, '41': 9, '42': 10,
                 '43': 11}
    label = {'phy': label_phy, 'info': label_info, 'all': label_all}

    # Transition matrix
    if args.model.lower() =='sir':
        param = get_param_SIR(args.beta_phy, args.gamma_phy, args.beta_pro, args.gamma_pro, args.beta_anti,
                                args.gamma_anti, args.influence_pro, args.influence_anti, label_phy, label_info)
    elif args.model.lower() == 'sirs':
        param = get_param_SIRS(args.beta_phy, args.gamma_phy, args.beta_pro, args.gamma_pro, args.beta_anti,
                              args.gamma_anti, args.tao, args.influence_pro, args.influence_anti, label_phy, label_info)

    else:
        sys.exit("Model not implemented")

    T, avgStateCount, epidemic_size, full_records, verbose_records = simulation(args, label, param)

    now = datetime.now()
    dt_string = str(now)
    fnm = "output_" + dt_string

    save_results(args.model, avgStateCount, epidemic_size, T, file_name=fnm, full_records=full_records, verbose_records=verbose_records)





