from collections import Counter
from networkx import nx
import numpy as np


def generate_network(network_type, N, k1=2, k2=8, prob=0.5):
    if network_type == 'Regular':       # 5-regular graph
        degree = np.ones(N, dtype=int) * 5
        G = nx.configuration_model(degree, create_using=nx.Graph())

    elif network_type == 'SF':
        v = 1.32
        kc = 35
        p = [np.power(k, -v) * np.exp(-k / kc) for k in np.arange(1, 50, 1)]
        p = [0] + p
        norm_p = p / np.sum(p)
        cdf_p = np.cumsum(norm_p)
        sum_degrees = 1
        while np.mod(sum_degrees, 2) == 1:
            degrees = [np.sum(cdf_p < np.random.rand()) for k in range(N)]
            sum_degrees = np.sum(degrees)
        degrees = np.random.permutation(degrees)
        G = nx.configuration_model(degrees, create_using=nx.Graph())

    elif network_type == 'Poisson':
        sum_degrees = 1
        while np.mod(sum_degrees, 2) == 1:
            degrees = np.random.poisson(5, N)
            sum_degrees = np.sum(degrees)
        G = nx.configuration_model(degrees, create_using=nx.Graph())

    elif network_type == '28':
        degrees = np.concatenate((np.ones(int(N / 2), dtype=int) * 2, np.ones(int(N / 2), dtype=int) * 8))
        degrees = np.random.permutation(degrees)
        G = nx.configuration_model(degrees, create_using=nx.Graph())

    elif network_type == 'Two_degrees':
        degrees = np.concatenate((np.ones(int(N * prob), dtype=int) * k1, np.ones(int(N * (1 - prob)), dtype=int) * k2))
        degrees = np.random.permutation(degrees)
        G = nx.configuration_model(degrees, create_using=nx.Graph())

    else:
        print("Method hasn't been implemented yet!")
        return

    cx = nx.to_scipy_sparse_matrix(G).tocoo()  # store nonzero data values with matrix coordinates
    N = G.number_of_nodes()

    Neighbor, I1 = get_neighbors(N, cx.col, cx.row)

    return [Neighbor, I1]


def get_neighbors(N, L1, L2):
    """
    :param N: number of nodes
    :param L1: row matrix coordinate of nonzero values
    :param L2: column matrix coordinate of nonzero values
    """
    index = np.argsort(L1)
    neighbor = L2[index]

    I1 = np.zeros(N + 1, dtype=int)
    degree = Counter(L1)  # count times nonzero values appear in each row
    for elt, count in degree.items():
        I1[elt + 1] = count
    I1 = np.cumsum(I1)

    return neighbor, I1



def para_degree_correlation(pk, a):
    k1 = int(pk[0, 0])
    k2 = int(pk[1, 0])
    r1 = k1 * pk[0, 1]
    r2 = k2 * pk[1, 1]
    z = r1 + r2
    max_degree = int(k2)
    Pkk = np.zeros((max_degree, max_degree))

    Pkk[k1 - 1, k1 - 1] = a
    Pkk[k1 - 1, k2 - 1] = r1 / z - a
    Pkk[k2 - 1, k1 - 1] = r1 / z - a
    Pkk[k2 - 1, k2 - 1] = r2 / z - r1 / z + a
    Pkk[Pkk < 0] = 0

    Pkk /= np.sum(Pkk)  # normalize to 1

    return Pkk



def FPkkGraph_gen(Pkk, Nnodes):
    # Given Pkk and the total number of nodes calculate how many edges of each type we need
    max_k = len(Pkk)

    Pk = np.sum(Pkk, axis=1) / np.arange(1, max_k+1)
    Pk = Pk / np.sum(Pk)
    Nedges_tot = np.dot(np.arange(1, max_k+1), Pk) * Nnodes

    Pkk_Nedges = Nedges_tot * Pkk

    # Because the network is undirected, we consider the part of Piikk_Nedges above the diagonal and half of the diagonal
    Pkk_Nedges_triu = np.round(np.triu(Pkk_Nedges, 1) + np.diag(np.diag(Pkk_Nedges)) / 2).astype(int)

    Nedges = np.sum(Pkk_Nedges_triu)  # number of edges we will generate

    # Allocate memory for
    k1k2 = np.zeros((Nedges, 2))  # matrix containing degree of nodes in n1n2

    insert_idx = 0  # index for edge insertion

    [k1_exist, k2_exist] = np.nonzero(
        Pkk_Nedges_triu)  # find positions of non - zero elements(edges with such degrees exist between modules i1 and i2)

    for idx in range(len(k1_exist)):  # loop over types of edges with various edge-end degrees
        m = int(Pkk_Nedges_triu[k1_exist[idx], k2_exist[idx]])
        tmp = (k1_exist[idx] + 1, k2_exist[idx] + 1)
        k1k2[insert_idx: insert_idx + m, :] = np.tile(tmp, (
            m, 1))  # insert these edge degrees in the list of all edge degrees
        insert_idx += m

    return k1k2, Pkk_Nedges_triu, Nedges



def FPkkGraph_pop(k1k2, Pkk_Nedges_triu,  Nedges):
    n1n2 = np.zeros((Nedges, 2), dtype=int)  # matrix containing edges(i.e., pairs of node numbers)
    NodeN_cur = 0


    [k1_exist, k2_exist] = np.nonzero(
        Pkk_Nedges_triu)  # find positions of non - zero elements(such degrees exist in module i1)
    k_exist_i1 = list(set(k1_exist) | set(k2_exist))  # degrees existing in module i1

    for k in k_exist_i1:  # loop over existing degrees,  k = degree - 1
        degree = k + 1
        idx_k = (k1k2 == degree)  # positions in i1i2, k1k2 of k - degree nodes of module i1
        N_edge_ends = np.sum(idx_k)
        Nnodes2assign = int(
            np.floor(N_edge_ends / degree))  # number of new k - degree nodes that we will create from these edge

        mismatched_local = np.mod(N_edge_ends, degree)

        temp = np.arange(NodeN_cur, NodeN_cur + Nnodes2assign)  # generate node numbers for nodes of degree

        NodeN_cur = NodeN_cur + Nnodes2assign  # update current node counter

        stubs_deg_k = np.random.permutation(
            np.tile(temp, (1, degree))[0])  # numbers of nodes to be assigned to n1n2
        stubs_deg_k = np.append(stubs_deg_k, np.ones(mismatched_local)*(-1))
        n1n2[idx_k] = stubs_deg_k  # assign node numbers to n1n2 in random order

    # Remove mismatched, self- and multi-connections
    n1n2 = np.delete(n1n2, np.where(n1n2[:, 0] == -1)[0], axis=0)
    n1n2 = np.delete(n1n2, np.where(n1n2[:, 1] == -1)[0], axis=0)
    n1n2 = np.delete(n1n2, np.where(n1n2[:, 0] == n1n2[:, 1])[0], axis=0)

    n1n2 = np.sort(n1n2, axis=1)
    n1n2 = np.unique(n1n2, axis=0)

    node_index = np.random.permutation(np.arange(0, NodeN_cur))

    n1n2[:, 0] = node_index[n1n2[:, 0]]
    n1n2[:, 1] = node_index[n1n2[:, 1]]

    n1n2 = np.append(n1n2, np.fliplr(n1n2), axis=0)
    return n1n2




def match_layers(n_info, n_phy, C, N):
    degree_phy = Counter(n_phy[:, 0])
    degree_info = Counter(n_info[:, 0])
    node_index_phy = np.array(list(degree_phy.keys()))
    degree_phy = np.array([degree_phy[x] for x in node_index_phy])
    node_index_info = np.array(list(degree_info.keys()))
    degree_info = np.array([degree_info[x] for x in node_index_info])

    l_info = C.shape[0]
    l_phy = C.shape[1]
    prob_info = np.sum(C, 1)
    prob_phy = np.sum(C, 0)
    C *= N

    # Partition nodes in each layer
    node_map_info = {}
    partition_nodes(degree_info, prob_info, l_info, l_phy, C, node_map_info)
    node_map_phy = {}
    partition_nodes(degree_phy, prob_phy, l_phy, l_info, np.transpose(C), node_map_phy)

    # Match the nodes from two layers
    ind = np.ones(N) * (-1)
    phy_unmatched = np.ones(N) * (-1)
    for k in range(l_info):
        for j in range(l_phy):
            if C[k, j]:
                l = min(len(node_map_info[(k, j)]), len(node_map_phy[(j, k)]))
                ind[node_index_info[node_map_info[(k, j)][:l]]] = node_index_phy[node_map_phy[(j, k)][:l]]
                phy_unmatched[node_index_phy[node_map_phy[(j, k)][:l]]] = 0

    unmatched_info = np.asarray(ind == -1).nonzero()[0]
    unmatched_phy = np.asarray(phy_unmatched == -1).nonzero()[0]

    ind[unmatched_info] = unmatched_phy
    n_info[:, 0] = ind[n_info[:, 0]]
    n_info[:, 1] = ind[n_info[:, 1]]

    net_info = get_neighbors(N, n_info[:, 0], n_info[:, 1])
    net_phy = get_neighbors(N, n_phy[:, 0], n_phy[:, 1])

    return net_info, net_phy



def partition_nodes(degree, degree_prob, l1, l2, C, node_map):
    for k in range(l1):
        dk = k + 1
        if degree_prob[k]:
            ind = (degree == dk).nonzero()[0]
            l_knodes = len(ind)
            prev = 0
            for j in range(l2):
                if C[k, j]:
                    cur = min(int(prev + C[k, j]), l_knodes)
                    node_map[(k, j)] = ind[prev: cur]
                    prev = cur


def para_inter_degree_correlation(pk_phy, pk_info, a):
    k1_phy = int(pk_phy[0, 0])
    k2_phy = int(pk_phy[1, 0])
    k1_phy_prob = pk_phy[0, 1]

    k1_info = int(pk_info[0, 0])
    k2_info = int(pk_info[1, 0])
    k1_info_prob = pk_info[0, 1]

    Pkk = np.zeros((k2_info, k2_phy))

    Pkk[k1_info - 1, k1_phy - 1] = a
    Pkk[k1_info - 1, k2_phy - 1] = k1_info_prob - a
    Pkk[k2_info - 1, k1_phy - 1] = k1_phy_prob - a
    Pkk[k2_info - 1, k2_phy - 1] = 1 - k1_info_prob - k1_phy_prob + a

    return Pkk

