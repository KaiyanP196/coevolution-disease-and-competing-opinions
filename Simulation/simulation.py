from utility import *
from network import *



def simulation(args, label, dynamics_para):
    """
    :param args: the input parameters
    :param label: Numeric labels for all the states
    :param dynamics_para: the parameters for the dynamics
    """
    tsize = int(args.RunTime / float(args.step))  # num. time steps
    t_interval = np.linspace(0, args.RunTime, num=tsize)  # time steps
    b = np.zeros((len(label['all']), tsize))  # compartments x time

    epidemic_size = []
    full_records = []
    verbose_records = {}

    if args.network_type_phy == 'Correlated':
        pk_phy = np.array([[args.k1_phy, args.k1_phy_prob], [args.k2_phy, (1 - args.k1_phy_prob)]])
        Pkk = para_degree_correlation(pk_phy, args.corr_a_phy)
        k1k2_phy, Pkk_Nedges_triu_phy, Nedges_phy = FPkkGraph_gen(Pkk, args.N)

    if args.network_type_info == 'Correlated':
        pk_info = np.array([[args.k1_info, args.k1_info_prob], [args.k2_info, (1 - args.k1_info_prob)]])
        Pkk = para_degree_correlation(pk_info, args.corr_a_info)
        k1k2_info, Pkk_Nedges_triu_info, Nedges_info = FPkkGraph_gen(Pkk, args.N)

    for index in range(args.num_sim):

        init_size = [int(args.N * args.initial_infected_pro), int(args.N * args.initial_infected_anti),
                     int(args.N * args.initial_infected_phy)]
        x0 = initialization(args.N, init_size, label['phy'], label['info'])

        if args.network_type_phy == 'Correlated':
            n1n2_phy = FPkkGraph_pop(k1k2_phy, Pkk_Nedges_triu_phy,
                                       Nedges_phy)
        else:
            net_phy = generate_network(args.network_type_phy, args.N, args.k1_phy, args.k2_phy,
                                       args.k1_phy_prob)

        if args.network_type_info == 'Correlated':
            n1n2_info = FPkkGraph_pop(k1k2_info, Pkk_Nedges_triu_info,
                                        Nedges_info)
        else:
            net_info = generate_network(args.network_type_info, args.N, args.k1_info, args.k2_info,
                                        args.k1_info_prob)

        if args.network_type_info == 'Correlated':
            C = para_inter_degree_correlation(pk_phy, pk_info, args.corr_a_inter)
            net_info, net_phy = match_layers(n1n2_info, n1n2_phy, C, args.N)

        ts, n_ind, i_ind, j_ind = simulation_dynamics(net_phy, net_info, args.N, dynamics_para, label, args.RunTime, x0)

        stateCount = post_process(x0, ts, i_ind, j_ind, label, args.N)
        y = np.zeros((len(label['all']), tsize))
        for step, t in enumerate(t_interval):
            ind = np.sum(ts <= t) - 1  # index of last event before time t
            y[:, step] = stateCount[:, ind] / args.N  # updated proportions of each compartment at time t

        recovery_size_phy = np.sum(y[8:12, -1])
        print(recovery_size_phy)
        epidemic_size.append(recovery_size_phy)

        if args.full_records:
            full_records.append(y)

        if args.verbose:
            verbose_records['n' + str(index)] = {'i_ind': i_ind, 'j_ind': j_ind, 'ts': ts}

        b += y  # add all proportions together (to be averaged after all simulations are finished)

    return t_interval, b / args.num_sim, epidemic_size, full_records, verbose_records



def simulation_dynamics(Net_phy, Net_info, N, dynamics_para, label, RunTime, x0):
    r_contact_phy = dynamics_para[0]
    r_contact_info = dynamics_para[2]

    label_phy = label['phy']
    label_info = label['info']

    neigh_phy = Net_phy[0]
    I1_phy = Net_phy[1]
    neigh_info = Net_info[0]
    I1_info = Net_info[1]

    n_ind = []  # list of nodes that experience events (in order of events)
    j_ind = []  # new state of chosen nodes
    i_ind = []  # original state of chosen nodes

    X = x0.astype(int)  # state vector

    nr_cont_phy = r_contact_phy.sum(axis=1)  # interaction transitions (event rates)
    nr_spon_phy =  dynamics_para[1].sum(axis=1)  # nodal (self) transitions (event rates)
    nr_spon_info = dynamics_para[3].sum(axis=1)

    phy_I = (X[0, :] == label_phy['I'])  # boolean vec. True if infected
    pro_I = (X[1, :] == label_info['P'])
    anti_I = (X[1, :] == label_info['A'])
    # num. infected neighbors of each node
    num_neigh_inf_phy = np.array([np.sum(phy_I[neigh_phy[I1_phy[n]:I1_phy[n + 1]]]) for n in range(N)])
    num_neigh_inf_pro = np.array([np.sum(pro_I[neigh_info[I1_info[n]:I1_info[n + 1]]]) for n in range(N)])
    num_neigh_inf_anti = np.array([np.sum(anti_I[neigh_info[I1_info[n]:I1_info[n + 1]]]) for n in range(N)])

    # event rate of each node
    Rn_phy = np.asarray([nr_spon_phy[X[0, n]] + nr_cont_phy[X[0, n]] * num_neigh_inf_phy[n] for n in range(N)])
    Rn_info = np.asarray([nr_spon_info[X[1, n]] + r_contact_info[X[1, n], label_info['P']] * num_neigh_inf_pro[n] + r_contact_info[
       X[1, n], label_info['A']] * num_neigh_inf_anti[n] for n in range(N)])

    # rate at which any event on any node occurs
    R_phy = np.sum(Rn_phy)  # event rate in physical layer only
    R_info = np.sum(Rn_info)  # event rate in information layer only

    Rtotal = R_phy + R_info  # event rate for entire system

    ts = [0]  # list of event times
    Tf = 0  # iterator (time passed)

    while Tf < RunTime:
        """
        Pick the next event following exponential distribution
        """

        t_rnd = -np.log(np.random.rand()) / Rtotal  # time until next event happens (inverse survival fxn.)
        Tf += t_rnd  # current time
        next_event = np.random.rand() < (R_phy / Rtotal)  # boolean, True if next event is physical

        if next_event:  # next event is in physical layer
            ns = np.random.choice(N, 1, p=Rn_phy / R_phy)[
                0]  # choose node that will change states, index at 0 to unwrap value from numpy array
            prev_phy = X[0, ns]  # prev. physical state of chosen node
            prev_info = cur_info = X[1, ns]  # prev. awareness state of chosen node unchanged

            if prev_phy == label_phy['I']:  # node was infected
                cur_phy = label_phy['R']  # Removed: chosen node will no longer experience phys. events

                Nln = neigh_phy[I1_phy[ns]:I1_phy[ns + 1]]  # physical neighbors of chosen node
                num_neigh_inf_phy[Nln] -= 1  # neighbors of chosen node have 1 less infected neighbor
                # update event rates to exclude chosen node
                temp_decrease = nr_cont_phy[X[0, Nln]]   # Only those neighbors who are in S will be affected
                Rn_phy[Nln] -= temp_decrease
                R_phy -= np.sum(temp_decrease)

            else:  # node was susceptible
                cur_phy = label_phy['I']

                Nln = neigh_phy[I1_phy[ns]:I1_phy[ns + 1]]  # physical neighbors of chosen node
                num_neigh_inf_phy[Nln] += 1  # neighbors of chosen node have 1 more infected neighbor
                # update event rates to include chosen node
                temp_add = nr_cont_phy[X[0, Nln]]
                Rn_phy[Nln] += temp_add
                R_phy += np.sum(temp_add)

            # Update states
            X[0, ns] = cur_phy
            R_phy -= Rn_phy[ns]  # subtract old event rate of chosen node
            Rn_phy[ns] = nr_spon_phy[cur_phy] + nr_cont_phy[cur_phy] * num_neigh_inf_phy[
                ns]  # calculate new event rate of chosen node
            R_phy += Rn_phy[ns]  # add new event rate of chosen node

        else:  # next event is in info layer
            ns = np.random.choice(N, 1, p=Rn_info / R_info)[0]
            prev_info = X[1, ns]  # prev. awareness state of chosen node
            prev_phy = cur_phy = X[0, ns]  # prev. physical state of chosen node (may remain unchanged)

            if prev_info == label_info['P']:  # node was pro-s.d.
                cur_info = label_info['R']
                # update physical layer if node was also physically susceptible
                if prev_phy == label_phy['SP']:
                    X[0, ns] = cur_phy = label_phy['SN']
                    # update event rates
                    R_phy -= Rn_phy[ns]
                    Rn_phy[ns] = nr_cont_phy[cur_phy] * num_neigh_inf_phy[ns]
                    R_phy += Rn_phy[ns]

                Nln = neigh_info[I1_info[ns]:I1_info[ns + 1]]
                num_neigh_inf_pro[Nln] -= 1
                # update event rates of neighbors to exclude chosen node
                temp_decrease = r_contact_info[X[1, Nln], label_info[
                    'P']]  # neighbors have one less pro-infected nodes => subtract event rates of being infected by pro-neighbors
                Rn_info[Nln] -= temp_decrease
                R_info -= np.sum(temp_decrease)

            elif prev_info == label_info['A']:  # node was anti-s.d.
                cur_info = label_info['R']

                # update physical layer if node was also physically susceptible
                if prev_phy == label_phy['SA']:
                    X[0, ns] = cur_phy = label_phy['SN']
                    # update event rates
                    R_phy -= Rn_phy[ns]
                    Rn_phy[ns] = nr_cont_phy[cur_phy] * num_neigh_inf_phy[ns]
                    R_phy += Rn_phy[ns]


                Nln = neigh_info[I1_info[ns]:I1_info[ns + 1]]
                num_neigh_inf_anti[Nln] -= 1
                # update event rates to exclude chosen node
                temp_decrease = r_contact_info[X[1, Nln], label_info['A']]
                Rn_info[Nln] -= temp_decrease
                R_info -= np.sum(temp_decrease)

            elif prev_info == label_info['U']:  # node was susceptible (non-compliant)

                # Pr(node --> P | node --> P or A)
                pro_inf = np.random.rand() < (r_contact_info[label_info['U'], label_info['P']] * num_neigh_inf_pro[ns] / Rn_info[ns])

                if pro_inf:  # susceptible node becomes pro-s.d.
                    cur_info = label_info['P']

                    # update physical layer if node was also physically susceptible
                    if prev_phy == label_phy['SN']:
                        X[0, ns] = cur_phy = label_phy['SP']
                        # update even rates
                        R_phy -= Rn_phy[ns]
                        Rn_phy[ns] = nr_cont_phy[cur_phy] * num_neigh_inf_phy[ns]
                        R_phy += Rn_phy[ns]

                    Nln = neigh_info[I1_info[ns]:I1_info[ns + 1]]
                    num_neigh_inf_pro[Nln] += 1
                    # update event rates to include chosen node
                    temp_add = r_contact_info[X[1, Nln], label_info['P']]
                    Rn_info[Nln] += temp_add
                    R_info += np.sum(temp_add)

                else:  # susceptible node becomes anti-s.d.
                    cur_info = label_info['A']

                    # update physical layer if node was also physically susceptible
                    if prev_phy == label_phy['SN']:
                        X[0, ns] = cur_phy = label_phy['SA']
                        # update even rates
                        R_phy -= Rn_phy[ns]
                        Rn_phy[ns] = nr_cont_phy[cur_phy] * num_neigh_inf_phy[ns]
                        R_phy += Rn_phy[ns]


                    Nln = neigh_info[I1_info[ns]:I1_info[ns + 1]]
                    num_neigh_inf_anti[Nln] += 1
                    # update event rates to include chosen node
                    temp_add = r_contact_info[X[1, Nln], label_info['A']]
                    Rn_info[Nln] += temp_add
                    R_info += np.sum(temp_add)

            else:
                cur_info = label_info['U']
                # Nothing changes on the physical layer


            # Update the event rate of the chosen node
            X[1, ns] = cur_info
            R_info -= Rn_info[ns]  # subtract old event rate
            Rn_info[ns] = nr_spon_info[cur_info] + r_contact_info[cur_info, label_info['P']] * num_neigh_inf_pro[ns] + r_contact_info[
                                                                                                              cur_info,
                                                                                                              label_info[
                                                                                                                  'A']] * \
                                                                                                          num_neigh_inf_anti[
                                                                                                              ns]  # new event rate
            R_info += Rn_info[ns]  # add new event rate

        n_ind.append(ns)
        j_ind.append(str(cur_phy) + str(cur_info))
        i_ind.append(str(prev_phy) + str(prev_info))
        ts.append(Tf)

        if np.sum(Rn_phy < 0):  # handle cases when event rates drop below 0 when updating states
            Rn_phy[Rn_phy < 0] = 0
            R_phy = np.sum(Rn_phy)
        elif np.sum(Rn_info < 0):
            Rn_info[Rn_info < 0] = 0
            R_info = np.sum(Rn_info)

        # update overall system event rate
        Rtotal = R_phy + R_info

        # check if no other events will occur
        if R_phy < 1e-6:
            break

    return ts, n_ind, i_ind, j_ind