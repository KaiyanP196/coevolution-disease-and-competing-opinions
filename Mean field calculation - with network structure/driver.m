%% Example 1 Networks with no intra-layer or inter-layer degree-degree correlations
% i.e., networks are generated from configuration model networks.

% Parameters for Dynamics
beta_pro = 0.6;
gamma_pro = 0.1;
alpha_pro = 0.1;

beta_anti  = 0.6;
gamma_anti = 0.1;
alpha_anti = 10;

beta_phy =0.6;
gamma_phy = 1;
beta_phy_pro = beta_phy * alpha_pro;
beta_phy_anti = beta_phy * alpha_anti;

initial_P = 0.005;
initial_A = 0.005;
initial_I = 0.01;

% Parameters for Network
info_type = "Poisson";
phy_type = "Poisson";

tspan = 20;

[t, S, I, R_phy, U, P, A, R_info] = f_PWA_wrapper(info_type, phy_type, beta_phy, gamma_phy, beta_pro, ...
gamma_pro, beta_anti, gamma_anti, beta_phy_pro, beta_phy_anti,...
initial_P, initial_A, initial_I,  tspan);

%% Example 2: Networks with intra-layer degree-degree correlations 
% and/or inter-layer degree-degree correlations

% Parameters for Dynamics
beta_pro = 0.6;
gamma_pro = 0.1;
alpha_pro = 0.1;

beta_anti  = 0.6;
gamma_anti = 0.1;
alpha_anti = 10;

beta_phy =0.6;
gamma_phy = 1;
beta_phy_pro = beta_phy * alpha_pro;
beta_phy_anti = beta_phy * alpha_anti;

initial_P = 0.005;
initial_A = 0.005;
initial_I = 0.01;

tspan = 20;

% Parameters for Networks
k1_phy=2;
k2_phy=8;
pk1_phy = 0.5;
k_phy = [k1_phy, pk1_phy; k2_phy, 1-pk1_phy];
z = k_phy(1,1)*k_phy(1,2) + k_phy(2,1)*k_phy(2,2);
min_a_intra_phy = max(0, k_phy(1,1)*k_phy(1,2) - k_phy(2,1)*k_phy(2,2))/z;
max_a_intra_phy = k_phy(1,1)*k_phy(1,2)/z;
a_intra_phy_range = linspace(min_a_intra_phy, max_a_intra_phy, 20);


k1_info=2;
k2_info=8;
pk1_info = 0.5;
k_info = [k1_info, pk1_info; k2_info, 1-pk1_info];


min_a_inter = max(0, pk1_phy + pk1_info - 1);
max_a_inter = min(pk1_phy, pk1_info);
a_inter_range = linspace(min_a_inter, max_a_inter, 20);

a_intra_info = 0.2;
a_intra_phy = 0.2;
a_inter = 0.5;


[t, S, I, R_phy, U, P, A, R_info] = f_PWA_wrapper_correlated(k_info, k_phy, ...
    beta_phy, gamma_phy, beta_pro, gamma_pro, beta_anti, gamma_anti, beta_phy_pro, beta_phy_anti,...
    a_intra_info, a_intra_phy, a_inter, initial_P, initial_A, initial_I, tspan);
