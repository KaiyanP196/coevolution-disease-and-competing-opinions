%% Generate_degree
% Return the degree and its probability 
% [k1, p1; k2, p2; ....; kn, p3]
function p = generate_degree(network_type)
    if network_type == "Regular"
        p =[5, 1];
    elseif network_type == "28"
        p = [2, 0.5; 8, 0.5];
    elseif network_type == "Poisson"
        mean_degree = 5;
        x = 1 : 50;
        p = poisspdf(x,mean_degree);
        p = p/sum(p);
        p = [x; p]';
    elseif network_type == "SF"
        v = 1.32;
        kc = 35;
        x = 1 :50;
        p = power(x, -v).* exp(-x/kc);
        p = p/sum(p);
        p = [x; p]';
    elseif network_type == "two_degree"
        p = pk;
    end
end
