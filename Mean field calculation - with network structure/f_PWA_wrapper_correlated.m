% Initialization for pair approxiamtion

function [t, S, I, R_phy, U, P, A, R_info] = f_PWA_wrapper_correlated_beta(k_info, k_phy, ...
    beta_phy, gamma_phy, beta_pro, gamma_pro, beta_anti, gamma_anti, beta_phy_pro, beta_phy_anti,...
    a_intra_info, a_intra_phy, a_inter, initial_P, initial_A, initial_I, tspan)
    

    mk_info = k_info(:,1)'*k_info(:, 2);
    mk_phy = k_phy(:,1)'*k_phy(:, 2);

    num_k_phy = size(k_phy, 1);
    num_k_info = size(k_info, 1);
 
    
    % Use interlayer correlation matrix
    interlayer_corr = inter_degree_correlation_matrix(k_info(:, 2), k_phy(:, 2), a_inter);
    
    US = interlayer_corr * (1-initial_A - initial_P) * (1-initial_I);
    UI = interlayer_corr *  (1-initial_A - initial_P) * initial_I;
    UR = zeros(size(UI));
    AS = interlayer_corr *  initial_A * (1-initial_I);
    AI = initial_A * initial_I;
    AR = initial_A * 0 ;
    PS = interlayer_corr *  initial_P * (1-initial_I);
    PI = initial_P * initial_I;
    PR = 0;
    RS = zeros(size(PS));
    RI = 0 * initial_I ;
    
    % Degree correlation
    Pkk_info = intra_degree_correlation_matrix(k_info, mk_info, a_intra_info);
    Pkk_phy = intra_degree_correlation_matrix(k_phy, mk_phy, a_intra_phy);
    S_I = (1 - initial_I) * initial_I * Pkk_phy * mk_phy;
    S_S = (1 - initial_I) * (1 - initial_I) * Pkk_phy * mk_phy;
    cond_phy = interlayer_corr * diag(1./sum(interlayer_corr, 1));
    for ite = 1 : num_k_info
        US_I(:, :, ite) = diag(cond_phy(ite,:)) * S_I * (1-initial_A - initial_P) ;
        US_S(:, :, ite) = diag(cond_phy(ite,:)) * S_S * (1-initial_A - initial_P) ;
        AS_I(:, :, ite) = diag(cond_phy(ite,:)) * S_I * initial_A;
        AS_S(:, :, ite) = diag(cond_phy(ite,:)) * S_S * initial_A;
        PS_I(:, :, ite) = diag(cond_phy(ite,:)) * S_I * initial_P;
        PS_S(:, :, ite) = diag(cond_phy(ite,:)) * S_S * initial_P;
    end
    
    US_I = permute(US_I, [3, 1, 2]);
    US_S = permute(US_S, [3, 1, 2]);
    AS_I = permute(AS_I, [3, 1, 2]);
    AS_S = permute(AS_S, [3, 1, 2]);
    PS_I = permute(PS_I, [3, 1, 2]);
    PS_S = permute(PS_S, [3, 1, 2]);
    RS_S = zeros(size(US_I)) ;
    RS_I = zeros(size(US_I)) ;


    initial_U = (1 - initial_P - initial_A);
    U_P =  initial_U * initial_P * Pkk_info * mk_info;
    U_U = initial_U * initial_U * Pkk_info * mk_info;
    U_A = initial_U * initial_A * Pkk_info * mk_info;
    cond_info = diag(1./sum(interlayer_corr, 2)) * interlayer_corr;
    for ite = 1:num_k_phy
        US_P(:, :, ite) = diag(cond_info(:,ite)) * U_P * ( 1 - initial_I);
        US_A(:, :, ite) = diag(cond_info(:,ite)) *U_A * ( 1 - initial_I);
        US_U(:, :, ite) = diag(cond_info(:,ite)) *U_U * ( 1 - initial_I);
        UI_U(:, :, ite) = diag(cond_info(:,ite)) *U_U * initial_I;
        UI_P(:, :, ite) = diag(cond_info(:,ite)) *U_P * initial_I;
        UI_A(:, :, ite) = diag(cond_info(:,ite)) *U_A * initial_I;
    end
    
    US_P = permute(US_P, [1, 3, 2]);
    US_A = permute(US_A, [1, 3, 2]);
    US_U = permute(US_U, [1, 3, 2]);
    UI_U = permute(UI_U, [1, 3, 2]);
    UI_P = permute(UI_P, [1, 3, 2]);
    UI_A = permute(UI_A, [1, 3, 2]);
    UR_U = zeros(size(US_P));
    UR_P = zeros(size(US_P));
    UR_A = zeros(size(US_P));

    y0 = [US(:); UI(:); UR(:); AS(:); AI; AR; PS(:); PI; PR; RS(:); RI ];
    y01 = [US_S(:); US_I(:); US_U(:); US_A(:); US_P(:); ...
        AS_S(:); AS_I(:); ...
        PS_S(:); PS_I(:); ...
        RS_S(:); RS_I(:); ...
        UI_U(:); UI_P(:); UI_A(:);
        UR_U(:); UR_P(:); UR_A(:)];

    % The (k2-1)/k2 matrix with 3 dimensions: info, phy, phy.
    mat_k2_3phy = repmat(((k_phy(:,1)-1)./k_phy(:,1))', [num_k_info, 1, num_k_phy]);
    mat_k1_3info = repmat(((k_info(:,1)-1)./k_info(:,1)), [1, num_k_phy, num_k_info]);
    mat_k3phy = permute(mat_k2_3phy, [1, 3, 2]);
    mat_k3info = permute(mat_k1_3info, [3, 2, 1]);


    %% Main
    y0 = [y0; y01];
    s_2 = num_k_info * num_k_phy;
    s_3phy = s_2 * num_k_phy;
    s_3info = s_2 * num_k_info;
    index = [s_2, s_2, s_2, s_2, 1, 1, s_2, 1,...
        1, s_2, 1, s_3phy, s_3phy, s_3info, s_3info, s_3info, s_3phy, s_3phy,...
        s_3phy, s_3phy, s_3phy, s_3phy, s_3info, s_3info, s_3info, s_3info, s_3info, s_3info];
    index = cumsum(index);
    opts = odeset('NonNegative',1:size(y0, 1));  
    tspan = 0:0.01:tspan;
    [t, y] = ode45(@(t, y) f_PWA(t, y, gamma_pro, gamma_anti, gamma_phy, beta_pro, beta_anti, beta_phy, ...
        num_k_info, num_k_phy, mat_k2_3phy, mat_k1_3info, mat_k3phy, mat_k3info, index, beta_phy_pro, beta_phy_anti), tspan, y0, opts);

    %% Post process
    US = sum(y(:, 1:index(1)), 2);
    UI = sum(y(:, index(1)+1: index(2)), 2);
    UR = sum(y(:, index(2)+1: index(3)), 2);
    AS = sum(y(:, index(3)+1: index(4)), 2);
    AI = y(:, index(5));
    AR = y(:, index(6));
    PS = sum(y(:, index(6)+1: index(7)), 2);
    PI = y(:, index(8));
    PR = y(:, index(9));
    RS = sum(y(:, index(9)+1: index(10)), 2);
    RI = y(:, index(11));

    U = US + UI + UR;
    A = AS + AI + AR;
    P = PS + PI + PR;
    R_info = 1 - U - P -A;

    S = US + PS + AS + RS;
    I = UI + PI + AI + RI;
    R_phy = 1 - S - I;    
end


function Pkk = intra_degree_correlation_matrix(k, mk, a)
    b = k(1,1) * k(1,2)/mk - a;
    d = k(2,1) * k(2,2)/mk - b;
    if d <0
        disp(d)
        d = 0;
        pause()
    end

    Pkk = [a, b; b, d];
end


function corr = inter_degree_correlation_matrix(pk_info, pk_phy, a)
    corr = zeros(2, 2);
    corr(1,1) = a;
    corr(1,2) = pk_info(1) - a;
    corr(2, 1) = pk_phy(1) - a;
    corr(2, 2) = 1 - pk_info(1) - pk_phy(1) + a;
    
end


