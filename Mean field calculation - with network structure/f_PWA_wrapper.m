function [t, S, I, R_phy, U, P, A, R_info] = f_PWA_wrapper(info_type, phy_type, beta_phy, gamma_phy, beta_pro, ...
    gamma_pro, beta_anti, gamma_anti, beta_phy_pro, beta_phy_anti,...
    initial_P, initial_A, initial_I,  tspan)
    
    k_info = generate_degree(info_type);
    k_phy = generate_degree(phy_type);
 
    mk_info = k_info(:,1)'*k_info(:, 2);
    mk_phy = k_phy(:,1)'*k_phy(:, 2);

    num_k_phy = size(k_phy, 1);
    num_k_info = size(k_info, 1);
    k_info_mat = repmat(k_info(:, 1), 1, num_k_phy);
    k_phy_mat = transpose(repmat(k_phy(:, 1), 1, num_k_info));

    U = k_info(:, 2)*(1-initial_A - initial_P);
    A = k_info(:, 2)* initial_A;
    P = k_info(:, 2) * initial_P;
    S = k_phy(:, 2)*(1-initial_I);
    I = k_phy(:, 2) * initial_I;

    % Assume no degree correlation
    US = U * S';
    UI = U * I';
    UR = zeros(size(UI));
    AS = A * S';
    AI = initial_A * initial_I;
    AR = initial_A * 0 ;
    PS = P * S';
    PI = initial_P * initial_I;
    PR = 0;
    RS = zeros(size(PS));
    RI = 0 * initial_I ;

    US_temp = US .* k_phy_mat;
    AS_temp = AS .* k_phy_mat;
    PS_temp = PS .* k_phy_mat;
    US_I = zeros(num_k_info, num_k_phy, num_k_phy);
    US_S = zeros(size(US_I));
    AS_S = zeros(size(US_I));
    AS_I = zeros(size(US_I));
    PS_S = zeros(size(US_I));
    PS_I = zeros(size(US_I));
    RS_S = zeros(size(US_I)) ;
    RS_I = zeros(size(US_I)) ;
    for ite = 1:num_k_phy
        US_I(:,:, ite) = US_temp * k_phy(ite, 1) * I(ite)/mk_phy;
        US_S(:,:, ite) = US_temp * k_phy(ite, 1) * S(ite)/mk_phy;
        AS_S(:,:, ite) = AS_temp * k_phy(ite, 1) * S(ite)/mk_phy;
        AS_I(:,:, ite) = AS_temp * k_phy(ite, 1) * I(ite)/mk_phy;
        PS_S(:,:, ite) = PS_temp * k_phy(ite, 1) * S(ite)/mk_phy;
        PS_I(:,:, ite) = PS_temp * k_phy(ite, 1) * I(ite)/mk_phy;
    end

    US_temp = US .* k_info_mat;
    UI_temp = AS .* k_info_mat;
    US_P = zeros(num_k_info, num_k_phy, num_k_info);
    US_A = zeros(size(US_P));
    US_U = zeros(size(US_P));
    UI_U = zeros(size(US_P));
    UI_P = zeros(size(US_P));
    UI_A = zeros(size(US_P));
    UR_U = zeros(size(US_P));
    UR_P = zeros(size(US_P));
    UR_A = zeros(size(US_P));
    for ite = 1:num_k_info
        US_P(:, :, ite) = US_temp * k_info(ite, 1)*P(ite)/mk_info;
        US_A(:, :, ite) = US_temp * k_info(ite, 1)*A(ite)/mk_info;
        US_U(:, :, ite) = US_temp * k_info(ite, 1)*U(ite)/mk_info;
        UI_U(:, :, ite) = UI_temp * k_info(ite, 1)*U(ite)/mk_info;
        UI_P(:, :, ite) = UI_temp * k_info(ite, 1)*P(ite)/mk_info;
        UI_A(:, :, ite) = UI_temp * k_info(ite, 1)*A(ite)/mk_info;
    end

    y0 = [US(:); UI(:); UR(:); AS(:); AI; AR; PS(:); PI; PR; RS(:); RI ];
    y01 = [US_S(:); US_I(:); US_U(:); US_A(:); US_P(:); ...
        AS_S(:); AS_I(:); ...
        PS_S(:); PS_I(:); ...
        RS_S(:); RS_I(:); ...
        UI_U(:); UI_P(:); UI_A(:);
        UR_U(:); UR_P(:); UR_A(:)];

    % The (k2-1)/k2 matrix with 3 dimensions: info, phy, phy.
    mat_k2_3phy = repmat(((k_phy(:,1)-1)./k_phy(:,1))', [num_k_info, 1, num_k_phy]);
    mat_k1_3info = repmat(((k_info(:,1)-1)./k_info(:,1)), [1, num_k_phy, num_k_info]);
    mat_k3phy = permute(mat_k2_3phy, [1, 3, 2]);
    mat_k3info = permute(mat_k1_3info, [3, 2, 1]);


    %% Main
    y0 = [y0; y01];
    s_2 = num_k_info * num_k_phy;
    s_3phy = s_2 * num_k_phy;
    s_3info = s_2 * num_k_info;
    index = [s_2, s_2, s_2, s_2, 1, 1, s_2, 1,...
        1, s_2, 1, s_3phy, s_3phy, s_3info, s_3info, s_3info, s_3phy, s_3phy,...
        s_3phy, s_3phy, s_3phy, s_3phy, s_3info, s_3info, s_3info, s_3info, s_3info, s_3info];
    index = cumsum(index);
    
    opts = odeset('NonNegative',1:size(y0, 1)); 
    t_ = [];
    US = [];
    UI = [];
    UR = [];
    AS = [];
    AI = [];
    AR = [];
    PS = [];
    PI = [];
    PR = [];
    RS = [];
    RI = [];
    
    for i = 1:tspan
        [t,y] = ode23(@(t, y) f_PWA(t, y, gamma_pro, gamma_anti, gamma_phy, beta_pro, beta_anti, beta_phy, ...
            num_k_info, num_k_phy, mat_k2_3phy, mat_k1_3info, mat_k3phy, mat_k3info, index, beta_phy_pro, beta_phy_anti), [i-1, i], y0, opts);
        t_ = [t_; t];
        y0 = y(end,:);

        US = [US; sum(y(:, 1:index(1)), 2)];
        UI = [UI; sum(y(:, index(1)+1: index(2)), 2)];
        UR = [UR; sum(y(:, index(2)+1: index(3)), 2)];
        AS = [AS; sum(y(:, index(3)+1: index(4)), 2)];
        AI = [AI; y(:, index(5))];
        AR = [AR; y(:, index(6))];
        PS = [PS; sum(y(:, index(6)+1: index(7)), 2)];
        PI = [PI; y(:, index(8))];
        PR = [PR; y(:, index(9))];
        RS = [RS; sum(y(:, index(9)+1: index(10)), 2)];
        RI = [RI; y(:, index(11))];
    end

    %% Post process
    t = t_;
    U = US + UI + UR;
    A = AS + AI + AR;
    P = PS + PI + PR;
    R_info = 1 - U - P -A;

    S = US + PS + AS + RS;
    I = UI + PI + AI + RI;
    R_phy = 1 - S - I;
   
end



