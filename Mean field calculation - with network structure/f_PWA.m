function dydt = f_PWA(t, y, gamma_pro, gamma_anti, gamma_phy, beta_pro, beta_anti, beta_phy, ...
    num_k_info, num_k_phy, mat_k2_3phy, mat_k1_3info, mat_k3phy, mat_k3info, index, beta_phy_pro, beta_phy_anti)

    US = reshape(y(1:index(1)), [num_k_info, num_k_phy]);
    UI = reshape(y(index(1)+1: index(2)), [num_k_info, num_k_phy]);
    UR = reshape(y(index(2)+1: index(3)), [num_k_info, num_k_phy]);
    AS = reshape(y(index(3)+1: index(4)), [num_k_info, num_k_phy]);
    AI = y(index(5));
    AR = y(index(6));
    PS = reshape(y(index(6)+1: index(7)), [num_k_info, num_k_phy]);
    PI = y(index(8));
    PR = y(index(9));
    RS = reshape(y(index(9)+1: index(10)), [num_k_info, num_k_phy]);
    RI = y(index(11));
    
    
    US_S = reshape(y(index(11)+1:index(12)), [num_k_info, num_k_phy, num_k_phy]);
    US_I = reshape(y(index(12)+1:index(13)), [num_k_info, num_k_phy, num_k_phy]);
    US_U = reshape(y(index(13)+1:index(14)), [num_k_info, num_k_phy, num_k_info]);
    US_A = reshape(y(index(14)+1:index(15)), [num_k_info, num_k_phy, num_k_info]);
    US_P = reshape(y(index(15)+1:index(16)), [num_k_info, num_k_phy, num_k_info]);
    AS_S = reshape(y(index(16)+1:index(17)), [num_k_info, num_k_phy, num_k_phy]);
    AS_I = reshape(y(index(17)+1:index(18)), [num_k_info, num_k_phy, num_k_phy]); 
    PS_S = reshape(y(index(18)+1:index(19)), [num_k_info, num_k_phy, num_k_phy]);
    PS_I = reshape(y(index(19)+1:index(20)), [num_k_info, num_k_phy, num_k_phy]);
    RS_S = reshape(y(index(20)+1:index(21)), [num_k_info, num_k_phy, num_k_phy]);
    RS_I = reshape(y(index(21)+1:index(22)), [num_k_info, num_k_phy, num_k_phy]);
    UI_U = reshape(y(index(22)+1:index(23)), [num_k_info, num_k_phy, num_k_info]);
    UI_P = reshape(y(index(23)+1:index(24)), [num_k_info, num_k_phy, num_k_info]);
    UI_A = reshape(y(index(24)+1:index(25)), [num_k_info, num_k_phy, num_k_info]);
    UR_U = reshape(y(index(25)+1:index(26)), [num_k_info, num_k_phy, num_k_info]);
    UR_P = reshape(y(index(26)+1:index(27)), [num_k_info, num_k_phy, num_k_info]);
    UR_A = reshape(y(index(27)+1:index(28)), [num_k_info, num_k_phy, num_k_info]);
   
    US_P_2 = squeeze(sum(US_P, 3));
    US_P_2_ephy = repmat(US_P_2, [1,1,num_k_phy]);
    US_P_2_einfo = repmat(US_P_2, [1,1,num_k_info]);
    
    US_A_2 = squeeze(sum(US_A, 3));
    US_A_2_ephy = repmat(US_A_2, [1, 1, num_k_phy]);
    US_A_2_einfo = repmat(US_A_2, [1, 1, num_k_info]);
    
    US_I_2 = squeeze(sum(US_I, 3));
    US_I_2_einfo = repmat(US_I_2, [1,1,num_k_info]);
    US_I_2_ephy = repmat(US_I_2, [1,1,num_k_phy]);
    
    AS_I_2 = squeeze(sum(AS_I, 3));
    AS_I_2_ephy = repmat(AS_I_2, [1, 1, num_k_phy]);
    
    PS_I_2 = squeeze(sum(PS_I, 3));
    PS_I_2_ephy = repmat(PS_I_2, [1, 1, num_k_phy]);
    
    RS_I_2 = squeeze(sum(RS_I, 3));
    RS_I_2_ephy = repmat(RS_I_2, [1, 1, num_k_phy]);
    
    UI_P_2 = squeeze(sum(UI_P, 3)); 
    UI_P_2_einfo = repmat(UI_P_2, [1, 1, num_k_info]); 
    
    UI_A_2 = squeeze(sum(UI_A, 3));
    UI_A_2_einfo = repmat(UI_A_2, [1, 1, num_k_info]); 
    
    UR_P_2 = squeeze(sum(UR_P, 3)); 
    UR_P_2_einfo = repmat(UR_P_2, [1, 1, num_k_info]); 
    
    UR_A_2 = squeeze(sum(UR_A, 3));
    UR_A_2_einfo = repmat(UR_A_2, [1, 1, num_k_info]); 
    
    US_I_1 = squeeze(sum(US_I_2, 1));
    PS_I_1 = squeeze(sum(PS_I_2, 1));
    AS_I_1 = squeeze(sum(AS_I_2, 1));
    RS_I_1 = squeeze(sum(RS_I_2, 1));
    US_P_1 = squeeze(sum(US_P_2, 2));
    UI_P_1 =  squeeze(sum(UI_P_2, 2));
    UR_P_1 =  squeeze(sum(UR_P_2, 2));
    US_A_1 = squeeze(sum(US_A_2, 2));
    UI_A_1 =  squeeze(sum(UI_A_2, 2));
    UR_A_1 =  squeeze(sum(UR_A_2, 2));
    

    AS_I_0 = sum(AS_I_2(:)); 
    PS_I_0 = sum(PS_I_2(:)); 
    RS_I_0 = sum(RS_I_2(:)); 
    UI_A_0 = sum(UI_A_2(:)); 
    UI_P_0 = sum(UI_P_2(:)); 
    UR_A_0 = sum(UR_A_2(:)); 
    UR_P_0 = sum(UR_P_2(:)); 
    
    S_1 = squeeze(sum(US + PS + AS + RS , 1));
    S_e3 = reshape(S_1, [1, 1, num_k_phy]);
    S_e3 = repmat(S_e3, [num_k_info, num_k_phy, 1]);
    U_1 = squeeze(sum(US + UI + UR , 2));
    U_e3 = reshape(U_1, [1, 1, num_k_info]);
    U_e3 = repmat(U_e3, [num_k_info, num_k_phy, 1]);
    
    S_I_1 = AS_I_1 + PS_I_1 + US_I_1 + RS_I_1;
    S_I_1 = reshape(S_I_1, [1, 1, num_k_phy]);
    S_I_e = repmat(S_I_1, [num_k_info, num_k_phy, 1]);
    U_P_1 = US_P_1 + UI_P_1 + UR_P_1;
    U_P_1 = reshape(U_P_1, [1, 1, num_k_info]);
    U_P_e = repmat(U_P_1, [num_k_info, num_k_phy, 1]);
    U_A_1 = US_A_1 + UI_A_1 + UR_A_1;
    U_A_1 = reshape(U_A_1, [1, 1, num_k_info]);
    U_A_e = repmat(U_A_1, [num_k_info, num_k_phy, 1]);
    
    

    dUSdt = - beta_phy * US_I_2 - beta_pro * US_P_2 - beta_anti * US_A_2;
    
    dUIdt = beta_phy * US_I_2 - gamma_phy * UI - beta_pro * UI_P_2 - beta_anti * UI_A_2;
    
    dURdt = gamma_phy * UI  - beta_pro * UR_P_2 - beta_anti* UR_A_2;
    
    dASdt = - beta_phy_anti * AS_I_2 + beta_anti * US_A_2 - gamma_anti * AS ;
    
    dAIdt = beta_phy_anti * AS_I_0 - gamma_phy * AI + beta_anti * UI_A_0 - gamma_anti * AI;
    
    dARdt = gamma_phy * AI + beta_anti * UR_A_0 - gamma_anti * AR;
    
    dPSdt = - beta_phy_pro * PS_I_2 + beta_pro * US_P_2 - gamma_pro * PS ;
    
    dPIdt = beta_phy_pro * PS_I_0 - gamma_phy * PI + beta_pro * UI_P_0 - gamma_pro * PI;
    
    dPRdt = gamma_phy * PI + beta_pro * UR_P_0 - gamma_pro * PR;
    
    dRSdt = -beta_phy * RS_I_2 + gamma_pro * PS + gamma_anti * AS;
    
    dRIdt = beta_phy * RS_I_0 - gamma_phy * RI + gamma_pro * PI + gamma_anti * AI;
    
    
    US_e_phy = repmat(US, [1, 1, num_k_phy]);
    US_e_info = repmat(US, [1, 1, num_k_info]);
    UI_e_info = repmat(UI, [1, 1, num_k_info]);
    UR_e_info = repmat(UR, [1, 1, num_k_info]);
    AS_e_phy = repmat(AS, [1, 1, num_k_phy]);
    PS_e_phy = repmat(PS, [1, 1, num_k_phy]);
    RS_e_phy = repmat(RS, [1, 1, num_k_phy]);
    


    I_US_I = mat_k2_3phy .* US_I_2_ephy.*US_I./US_e_phy; 
    I_US_S = mat_k2_3phy .* US_S .* US_I_2_ephy ./ US_e_phy;
    I_US_I = fix_nan(I_US_I);
    I_US_S = fix_nan(I_US_S);
    
    

    P_US_U = mat_k1_3info .* US_U .* US_P_2_einfo./US_e_info;
    A_US_U = mat_k1_3info .* US_U .* US_A_2_einfo ./ US_e_info;
    P_US_P = mat_k1_3info .* US_P .* US_P_2_einfo ./ US_e_info;
    A_US_P = mat_k1_3info .* US_P .* US_A_2_einfo ./ US_e_info;
    P_US_A = mat_k1_3info .* US_A .* US_P_2_einfo ./ US_e_info;
    A_US_A = mat_k1_3info .* US_A .* US_A_2_einfo ./ US_e_info;
    P_US_U = fix_nan(P_US_U);
    A_US_U = fix_nan(A_US_U);
    P_US_P = fix_nan(P_US_P);
    A_US_P = fix_nan(A_US_P);
    P_US_A = fix_nan(P_US_A);
    A_US_A = fix_nan(A_US_A);
    
    
    

    P_UI_P = mat_k1_3info .* UI_P .* UI_P_2_einfo ./ UI_e_info;
    A_UI_P = mat_k1_3info .* UI_P .* UI_A_2_einfo ./ UI_e_info;
    A_UI_A = mat_k1_3info .* UI_A .* UI_A_2_einfo ./ UI_e_info;
    P_UI_A = mat_k1_3info .* UI_A .* UI_P_2_einfo ./ UI_e_info; 
    A_UI_U = mat_k1_3info .* UI_U .* UI_A_2_einfo ./ UI_e_info;
    P_UI_U = mat_k1_3info .* UI_U .* UI_P_2_einfo ./ UI_e_info;
    P_UI_P = fix_nan(P_UI_P);
    A_UI_P = fix_nan(A_UI_P);
    A_UI_A = fix_nan(A_UI_A);
    P_UI_A = fix_nan(P_UI_A);
    A_UI_U = fix_nan(A_UI_U);
    P_UI_U = fix_nan(P_UI_U);
    
   

    I_AS_I = mat_k2_3phy .* AS_I .* AS_I_2_ephy ./ AS_e_phy;
    I_AS_S = mat_k2_3phy .* AS_S .* AS_I_2_ephy ./ AS_e_phy;
    I_AS_I = fix_nan(I_AS_I);
    I_AS_S = fix_nan(I_AS_S);
    

    I_PS_I = mat_k2_3phy .* PS_I .* PS_I_2_ephy ./ PS_e_phy;
    I_PS_S = mat_k2_3phy .* PS_S .* PS_I_2_ephy ./ PS_e_phy;
    I_PS_I = fix_nan(I_PS_I);
    I_PS_S = fix_nan(I_PS_S);
    

    I_RS_S = mat_k2_3phy .* RS_S .* RS_I_2_ephy ./ RS_e_phy;
    I_RS_I = mat_k2_3phy .* RS_I .* RS_I_2_ephy ./ RS_e_phy;
    I_RS_I = fix_nan(I_RS_I);
    I_RS_S = fix_nan(I_RS_S);
    

    P_UR_U = mat_k1_3info .* UR_U .* UR_P_2_einfo ./ UR_e_info;
    A_UR_U = mat_k1_3info .* UR_U .* UR_A_2_einfo ./ UR_e_info;
    P_UR_P = mat_k1_3info .* UR_P .* UR_P_2_einfo ./ UR_e_info;
    A_UR_P = mat_k1_3info .* UR_P .* UR_A_2_einfo ./ UR_e_info;
    P_UR_A = mat_k1_3info .* UR_A .* UR_P_2_einfo ./ UR_e_info;
    A_UR_A = mat_k1_3info .* UR_A .* UR_A_2_einfo ./ UR_e_info;
    P_UR_U = fix_nan(P_UR_U);
    A_UR_U = fix_nan(A_UR_U);
    P_UR_P = fix_nan(P_UR_P);
    A_UR_P = fix_nan(A_UR_P);
    P_UR_A = fix_nan(P_UR_A);
    A_UR_A = fix_nan(A_UR_A);
    
  
    

    P_US_I = US_I .* US_P_2_ephy./US_e_phy;
    A_US_I = US_I .* US_A_2_ephy./US_e_phy;
    P_US_S = US_S .* US_P_2_ephy./US_e_phy;
    A_US_S = US_S .* US_A_2_ephy./US_e_phy;
    P_US_I = fix_nan(P_US_I);
    A_US_I = fix_nan(A_US_I);
    P_US_S = fix_nan(P_US_S);
    A_US_S = fix_nan(A_US_S);
    
    
    I_US_P = US_P .* US_I_2_einfo./US_e_info;
    I_US_A = US_A .* US_I_2_einfo./US_e_info;
    I_US_U = US_U .* US_I_2_einfo./US_e_info;    
    I_US_P = fix_nan(I_US_P);
    I_US_A = fix_nan(I_US_A);
    I_US_U = fix_nan(I_US_U);
    
    US_S_I = mat_k3phy .* US_S .* S_I_e./S_e3;
    AS_S_I = mat_k3phy .* AS_S .* S_I_e ./ S_e3;
    PS_S_I = mat_k3phy .* PS_S .* S_I_e ./ S_e3;
    RS_S_I = mat_k3phy .* RS_S .* S_I_e ./ S_e3;
    US_U_P = mat_k3info .* US_U .* U_P_e./U_e3;
    US_U_A = mat_k3info .* US_U .* U_A_e./U_e3;
    UI_U_P = mat_k3info .* UI_U .* U_P_e./U_e3;
    UI_U_A = mat_k3info .* UI_U .* U_A_e./U_e3;
    UR_U_A = mat_k3info .* UR_U .* U_A_e ./ U_e3;
    UR_U_P = mat_k3info .* UR_U .* U_P_e ./ U_e3;
    
 
    beta_s = squeeze(sum(I_US_S*beta_phy + I_PS_S * beta_phy_pro + I_AS_S * beta_phy_anti + I_RS_S*beta_phy, 1))./ ...
        squeeze(sum(I_US_S + I_PS_S + I_AS_S + I_RS_S, 1));
    beta_s = permute(beta_s, [2,1]);
    beta_s = fix_nan(beta_s);
    beta_s = reshape(beta_s, [1, num_k_phy, num_k_phy]);
    beta_s_e = repmat(beta_s, [num_k_info, 1, 1]);


    dUS_Idt = -beta_phy * I_US_I - gamma_phy * US_I - beta_phy * US_I ...
        + beta_s_e.* US_S_I - beta_pro * P_US_I - beta_anti * A_US_I;
    dUS_Sdt = -beta_s_e.* US_S_I - beta_phy * I_US_S - beta_pro * P_US_S - beta_anti * A_US_S;
    dUS_Pdt = -gamma_pro * US_P + beta_pro * US_U_P - beta_pro * US_P - beta_pro * P_US_P - beta_anti * A_US_P - beta_phy * I_US_P;
    dUS_Adt = -gamma_anti * US_A + beta_anti * US_U_A - beta_anti * US_A - beta_anti * A_US_A - beta_pro * P_US_A - beta_phy * I_US_A;
    dUS_Udt = - beta_pro * US_U_P - beta_anti * US_U_A - beta_phy * I_US_U - beta_pro * P_US_U - beta_anti * A_US_U;
    
    dUI_Pdt = -gamma_pro * UI_P + beta_pro * UI_U_P - beta_pro * UI_P - beta_pro * P_UI_P - beta_anti * A_UI_P + beta_phy * I_US_P - gamma_phy * UI_P;
    dUI_Adt = -gamma_anti * UI_A + beta_anti * UI_U_A - beta_anti * UI_A - beta_pro * P_UI_A - beta_anti * A_UI_A + beta_phy * I_US_A - gamma_phy * UI_A;
    dUI_Udt = -gamma_phy * UI_U + beta_phy * I_US_U - beta_anti * UI_U_A - beta_anti * A_UI_U - beta_pro * UI_U_P - beta_pro * P_UI_U;
    
    dUR_Pdt = -gamma_pro * UR_P + beta_pro * UR_U_P - beta_pro * UR_P - beta_pro * P_UR_P - beta_anti * A_UR_P + gamma_phy * UI_P;
    dUR_Adt = -gamma_anti * UR_A + beta_anti * UR_U_A - beta_anti * UR_A - beta_pro * P_UR_A - beta_anti * A_UR_A + gamma_phy * UI_A;
    dUR_Udt =  -beta_anti * UR_U_A - beta_pro * UR_U_P - beta_anti * A_UR_U - beta_pro * P_UR_U + gamma_phy * UI_U;
    
    dAS_Idt = - gamma_phy * AS_I - beta_phy_anti * AS_I - beta_phy_anti * I_AS_I ...
        + beta_s_e .* AS_S_I - gamma_anti * AS_I + beta_anti * A_US_I ;
    dAS_Sdt = -beta_s_e .* AS_S_I - beta_phy_anti * I_AS_S - gamma_anti * AS_S + beta_anti * A_US_S;
    
    dPS_Idt = - gamma_phy * PS_I - beta_phy_pro * PS_I - beta_phy_pro * I_PS_I ...
        + beta_s_e .* PS_S_I - gamma_pro * PS_I + beta_pro * P_US_I ;
    dPS_Sdt = -beta_s_e.* PS_S_I - beta_phy_pro * I_PS_S - gamma_pro * PS_S  + beta_pro * P_US_S;
    
    dRS_Idt = - gamma_phy * RS_I - beta_phy * RS_I - beta_phy * I_RS_I ...
        + beta_s_e .* RS_S_I + gamma_pro * PS_I + gamma_anti * AS_I;
    dRS_Sdt = -beta_s_e .* RS_S_I - beta_phy * I_RS_S + gamma_pro * PS_S + gamma_anti * AS_S;
    
    dydt = [dUSdt(:); dUIdt(:); dURdt(:); dASdt(:); dAIdt; dARdt; dPSdt(:); dPIdt; dPRdt; dRSdt(:); dRIdt;...
        dUS_Sdt(:); dUS_Idt(:); dUS_Udt(:); dUS_Adt(:); dUS_Pdt(:); ...
        dAS_Sdt(:); dAS_Idt(:); ...
        dPS_Sdt(:); dPS_Idt(:); ...
        dRS_Sdt(:); dRS_Idt(:); ...
        dUI_Udt(:); dUI_Pdt(:); dUI_Adt(:);
        dUR_Udt(:); dUR_Pdt(:); dUR_Adt(:)];    
   
end


function x = fix_nan(x)
    x(isnan(x)) = 0;
    x(isinf(x)) = 0;
end