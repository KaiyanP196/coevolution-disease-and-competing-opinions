# A Multilayer Network Model of the Coevolution of the Spread of a Disease and Competing Opinions

This repository contains code for the paper "A Multilayer Network Model of the Coevolution of the Spread of a Disease and Competing Opinions" by Kaiyan Peng, Zheng Lu, Vanessa Lin, Michael R. Lindstrom, Christian Parkinson, Chuntian Wang, Andrea L. Bertozzi and Mason A. Porter.

The "Simulation" folder provides Python code for simulating our full model with a Gillespie algorithm. Please find sample usage in "run.sh".

The "Mean field calculation - with network structure" folder provides Matlab code for computing pair approximation. See "driver.m" for sample usage.

The "Mean field calculation - fully mixed population" folder provides Matlab code for computing mean-field dynamics on a fully-mixed population. 

## Note on initial conditions

In Figure 4, we consider $`I_0 = 1 \times 10^{-6}`$. We specify the values for $`A_0`$ and $`P_0`$ in the legend. For the rest of the experiments in our paper, we set $`I_0 = 1\times 10^{-2}`$ and $`P_0 = A_0 = 5\times 10^{-3}`$. 


