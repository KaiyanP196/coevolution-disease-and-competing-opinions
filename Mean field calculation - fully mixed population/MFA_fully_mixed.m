%% Initial conditions
i0 = 1e-6;
s0= 1- i0;

a0 = 1e-6;
p0 = a0;
u0 = 1 - p0 - a0;
y0= [s0, i0, u0, p0, a0];

%% Parameters
beta_phy = 1;
gamma_phy = 0.5;

beta_pro = 2;
gamma_pro = 0.2;
alpha_pro = 0.1;

beta_anti  = 2;
gamma_anti = 0.2;
alpha_anti = 10;
    
opts = odeset('NonNegative',1:size(y0, 1));  
tspan = 0:0.01:60;

%% Solver
[t,y] = ode45 (@(t, y) f_compartmental_model(t, y, gamma_pro, gamma_anti, gamma_phy, beta_pro, beta_anti, beta_phy, ...
    alpha_pro, alpha_anti), tspan, y0, opts);


function dydt = f_compartmental_model(t, y, gamma_pro, gamma_anti, gamma_phy, beta_pro, beta_anti, beta_phy, alpha_pro, alpha_anti)
    % y = [S, I, U, P, A]
    dydt = zeros(size(y));
    S = y(1);
    I = y(2);
    U = y(3);
    P = y(4);
    A = y(5);
    
    beta = (P * alpha_pro + A * alpha_anti +  1 - P - A) * beta_phy;
    dydt(1) = -beta * S * I;
    dydt(2) = beta * S * I - gamma_phy * I;
    
    dydt(3) = -beta_pro * U * P - beta_anti * U * A;
    dydt(4) = beta_pro * U * P - gamma_pro * P;
    dydt(5) = beta_anti * U * A - gamma_anti * A;
end